package com.example.mpaytask1;

import com.example.mpaytask1.test.ListExample;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.List;

@SpringBootApplication
@EnableFeignClients
@EnableScheduling
@RequiredArgsConstructor
public class MPayTask1Application implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(MPayTask1Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

    }

}
