package com.example.mpaytask1.scheduler;

import com.example.mpaytask1.service.SchedulerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Log4j2
@RequiredArgsConstructor
public class StoreScheduler {

    private final SchedulerService schedulerService;

    @Scheduled(cron = "${application.scheduler.cron.expression}") // Her gun 10.30 da
    public void storeCurrencyForToday() throws Exception {
        schedulerService.storeValuteList();
    }
}
