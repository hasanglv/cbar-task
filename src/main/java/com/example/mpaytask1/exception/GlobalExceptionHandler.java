package com.example.mpaytask1.exception;

import com.example.mpaytask1.exception.custom.CurrencyNotFoundException;
import com.example.mpaytask1.exception.custom.DataRetrievalException;
import com.example.mpaytask1.exception.custom.InvalidDateException;
import com.example.mpaytask1.exception.custom.ParsingException;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.util.List;
import java.util.Map;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler extends DefaultErrorAttributes {

    @SneakyThrows
    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleException(Exception ex) {
        log.error("An error occurred: {}", ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body("Internal server error occurred: " + ex.getMessage());
    }


    @SneakyThrows
    @ExceptionHandler(DataRetrievalException.class)
    public ResponseEntity<?> handleDataException(DataRetrievalException ex) {
        log.error("Service unavailable due to data retrieval error: {}", ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
                .body("Service unavailable due to data retrieval error: " + ex.getMessage());
    }

    @SneakyThrows
    @ExceptionHandler(CurrencyNotFoundException.class)
    public ResponseEntity<?> handleCurrencyNotFoundException(CurrencyNotFoundException exception) {
        log.error("Unvailable currency requested: {}", exception.getMessage(), exception);
        return ResponseEntity.
                status(HttpStatus.NOT_FOUND).body(exception.getMessage());
    }

    @SneakyThrows
    @ExceptionHandler(ParsingException.class)
    public ResponseEntity<?> handleParseException(ParsingException ex) {
        log.error("Parsing error: {}", ex.getMessage(), ex);
        return ResponseEntity
                .status(HttpStatus.CONFLICT)
                .body("Parsing error occurred: " + ex.getMessage());
    }

    @SneakyThrows
    @ExceptionHandler(InvalidDateException.class)
    public ResponseEntity<?> handleInvalidDateException(InvalidDateException ex) {
        log.error("Invalid date requested: {}", ex.getMessage(), ex);
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

    private ResponseEntity<Map<String, Object>> ofType(WebRequest request, HttpStatus status,
                                                       String message,
                                                       List<ConstraintsViolationError> validationErrors) {
        Map<String, Object> attributes = getErrorAttributes(request, ErrorAttributeOptions.defaults());
        attributes.put("status", status.value());
        attributes.put("error", message);
        attributes.put("fieldErrors", validationErrors);
        attributes.put("path", ((ServletWebRequest) request).getRequest().getRequestURI());
        return new ResponseEntity<>(attributes, status);
    }

}
