package com.example.mpaytask1.exception.custom;

public class ParsingException extends RuntimeException {

    public ParsingException(String message) {
        super(message);
    }

    public ParsingException(Throwable throwable) {
        super(throwable);
    }
}
