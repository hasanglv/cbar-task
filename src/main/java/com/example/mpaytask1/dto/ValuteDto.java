package com.example.mpaytask1.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ValuteDto /*USD,EURO*/ {

    @JsonProperty("Code")
    private String code;

    @JsonProperty("Nominal")
    private String nominal;

    @JsonProperty("Name")
    private String name;

    @JsonProperty("Value")
    private String value;
}

