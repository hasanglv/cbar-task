package com.example.mpaytask1.util;


import com.example.mpaytask1.dto.CurrencyRatesDto;
import com.example.mpaytask1.dto.TypesOfValuteDto;
import com.example.mpaytask1.dto.ValuteDto;
import com.example.mpaytask1.exception.custom.ParsingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
public class ConverterUtil {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final XmlMapper xmlMapper = new XmlMapper();

    public static CurrencyRatesDto convertJSONToDto(String json, Class<CurrencyRatesDto> currencyRatesDtoClass) throws Exception {
        try {
            log.info("Converting JSON to DTO");
            return objectMapper.readValue(json, CurrencyRatesDto.class);
        } catch (Exception exception) {
            throw new ParsingException("Failed to convert JSON TO DTO");
        }

    }

    public static String convertXMLtoJSON(String xmlContent) throws Exception {
        try {
            log.info("Converting XML to JSON");
            Object xmlObject = xmlMapper.readValue(xmlContent, Object.class);
            return objectMapper.writeValueAsString(xmlObject);
        } catch (Exception exception) {
            throw new ParsingException("Failed to convert XML to JSON");
        }
    }

    public static List<ValuteDto> convertCurrencyToValute(CurrencyRatesDto currencyRatesDto) throws Exception {
        List<ValuteDto> valuteDtoList = new ArrayList<>();
        for (TypesOfValuteDto typesOfValuteDto : currencyRatesDto.getTypesOfValuteDtos()) {
            valuteDtoList.addAll(typesOfValuteDto.getValuteDtos());
        }
        return valuteDtoList;
    }
}
