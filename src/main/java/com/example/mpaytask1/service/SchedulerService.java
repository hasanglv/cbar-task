package com.example.mpaytask1.service;

import com.example.mpaytask1.dto.ValuteDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SchedulerService {

    private final CbarService cbarService;
    private final CacheService cacheService;


    public void storeValuteList() {
        List<ValuteDto> valuteDtos = cbarService.retrieveValuteListByDayFromApi(CurrencyService.TODAY_DATE);
        cacheService.setValuteDtoListByDate(CurrencyService.TODAY_DATE, valuteDtos);
    }
}
