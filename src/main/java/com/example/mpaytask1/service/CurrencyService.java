package com.example.mpaytask1.service;

import com.example.mpaytask1.dto.ValuteDto;
import com.example.mpaytask1.exception.custom.InvalidDateException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CurrencyService {

    private final CbarService cbarService;
    private final CacheService cacheService;
    public static String TODAY_DATE =LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");


    public List<ValuteDto> getCurrencyByDateAndCode(String date, String currency) {
        validateDate(date); // Validate the date before proceeding

        if (currency == null || currency.isEmpty()) {
            var data = cacheService.retrieveByDate(date);
            if (data == null) {
                var valuteDtos = cbarService.retrieveValuteListByDayFromApi(date);
                cacheService.setValuteDtoListByDate(date, valuteDtos);
                return valuteDtos;
            }
            return data;
        }
        ValuteDto valuteDto = cacheService.retrieveByDateAndCurrency(date, currency);
        if (valuteDto == null) {
            ValuteDto valuteDto1 = cbarService.retrieveValuteByDayFromApi(date, currency);
            cacheService.setValuteByDate(date, currency, valuteDto1);
            return List.of(valuteDto1);
        }

        return List.of(valuteDto);
    }

    public List<ValuteDto> getTodayCurrencyByDateAndCode(String currency) {
        return getCurrencyByDateAndCode(TODAY_DATE,currency);
    }

    private void validateDate(String date) {
        LocalDate parsedDate;
        try {
            parsedDate = LocalDate.parse(date, DATE_FORMATTER);
        } catch (DateTimeParseException e) {
            throw new InvalidDateException("Invalid date format: " + date);
        }

        if (parsedDate.isAfter(LocalDate.now())) {
            throw new InvalidDateException("Date cannot be in the future: " + date);
        }
    }



}
