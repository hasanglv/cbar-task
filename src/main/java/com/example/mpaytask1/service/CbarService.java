package com.example.mpaytask1.service;

import com.example.mpaytask1.dto.CurrencyRatesDto;
import com.example.mpaytask1.dto.ValuteDto;
import com.example.mpaytask1.exception.custom.CurrencyNotFoundException;
import com.example.mpaytask1.exception.custom.DataRetrievalException;
import com.example.mpaytask1.feign.CBARFeignClient;
import com.example.mpaytask1.util.ConverterUtil;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;


@Slf4j
@Service
@RequiredArgsConstructor
public class CbarService {


    private final CBARFeignClient cbarFeignClient;
    static String todayDate = LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));

    @SneakyThrows
    public List<ValuteDto> retrieveValuteListByDayFromApi(String date) {
        try {
            String xmlContent = cbarFeignClient.getCurrencies(date);
            String jsonFile = ConverterUtil.convertXMLtoJSON(xmlContent);
            CurrencyRatesDto currencyRatesDto = ConverterUtil.convertJSONToDto(jsonFile, CurrencyRatesDto.class);
            return ConverterUtil.convertCurrencyToValute(currencyRatesDto);
        } catch (Exception exception) {
            throw new DataRetrievalException("Failed to retrieve data from API");
        }

    }


    @SneakyThrows
    public ValuteDto retrieveValuteByDayFromApi(String date, String currencyCode) {
        String xmlContent = cbarFeignClient.getCurrencies(date);
        String jsonFile = ConverterUtil.convertXMLtoJSON(xmlContent);
        CurrencyRatesDto currencyRatesDto = ConverterUtil.convertJSONToDto(jsonFile, CurrencyRatesDto.class);
        List<ValuteDto> valuteDtos = ConverterUtil.convertCurrencyToValute(currencyRatesDto);
        return valuteDtos.stream()
                .filter(valuteDto -> valuteDto.getCode().equals(currencyCode))
                .findFirst()
                .orElseThrow(() -> new CurrencyNotFoundException("Currency code not found: " + currencyCode));
    }




}
