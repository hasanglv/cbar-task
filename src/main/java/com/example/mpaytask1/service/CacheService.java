package com.example.mpaytask1.service;

import com.example.mpaytask1.dto.ValuteDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.bootstrap.encrypt.KeyProperties;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Service
@Slf4j
@RequiredArgsConstructor
public class CacheService {


    private final RedisTemplate<String, ValuteDto> redisTemplate;

    public List<ValuteDto> retrieveByDate(String date) {
        Long size = redisTemplate.opsForList().size(date);
        if (size == null || size < 25) {
            return null;
        }
        return redisTemplate.opsForList().range(date, 0, -1);
    }

    public ValuteDto retrieveByDateAndCurrency(String date, String currency) {
        List<ValuteDto> valuteDtoList = redisTemplate.opsForList().range(date, 0, -1);
        if (valuteDtoList == null) {
            return null;
        }
        return valuteDtoList.stream()
                .filter(valuteDto -> currency.equals(valuteDto.getCode()))
                .findFirst()
                .orElse(null);
    }

    public void setValuteByDate(String date, String currencyCode, ValuteDto valuteDto) {
        redisTemplate.opsForList().rightPush(date, valuteDto);
    }

    public void setValuteDtoListByDate(String date, List<ValuteDto> valuteDtoList) {
        redisTemplate.opsForList().rightPushAll(date, valuteDtoList);
    }



}
